

class RegisterForm{
	constructor(selector, useService){
		this.selector = selector;
		this.userService = userService;
		this.onRegister = ()=>{}

		document.addEventListener("DOMContentLoaded",()=>{
		 	this.init();
		 	this.binds();
		 });
	}


	init(){
		this.container = document.querySelector(this.selector);
		this.loginInput = this.container.querySelector("#login_user_login");
		this.passwordInput = this.container.querySelector("#login_user_password");
		this.bornInput = this.container.querySelector("#login_user_born");
		this.button = this.container.querySelector("button");
	}

	binds(){
		this.button.addEventListener('click', ()=>this.register())
	}

	register(){
		let user = new User(
			this.loginInput.value,
			this.passwordInput.value,
			this.bornInput.value
		);

		this.userService.register(user).then(r=>{
			if (r.status === "error") this.registerError(r.error);
			else this.successRegister();
		})

	}

	registerError(text){
		alert(text);
	}

	successRegister(){
		this.clearForm();
		alert("Вы зарегистрировались!");
		this.onRegister();
	}

	clearForm(){
		this.passwordInput.value = "";
		this.bornInput.value = "";
		this.loginInput.value = "";
	}

	// hide(){
	// 	this.container.style.displfy = "none";
	// }
}









