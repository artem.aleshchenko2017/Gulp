
let project_folder = "dist";
let sourse_folder = "src";

let path = {
	build:{
		html : project_folder + "/",
		css : project_folder + "/css/",
		js : project_folder + "/js/",
		fonts : project_folder + "/fonts/",
	},
	src:{
		html : sourse_folder + "/index.html",
		css : sourse_folder + "/style/style.sass",
		js : sourse_folder + "/script/**/*.js",
		// fonts : sourse_folder + "/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
	},
	watch:{
		html : sourse_folder + "/index.html",
		css : sourse_folder + "/style/style.sass",
		js : sourse_folder + "/script/**/*.js",
	},
	clean : "./" + project_folder + "/"
}

let {src, dest} = require('gulp'),
	gulp = require('gulp'),
	browsersync = require('browser-sync').create(),
	fileinclude = require("gulp-file-include"),
	del = require("del"),
	sass = require('gulp-sass')(require('sass')),
	autoprefixer = require("gulp-autoprefixer"),
	group_media = require("gulp-group-css-media-queries"),
	clean_css = require("gulp-clean-css"),
	rename = require("gulp-rename"),
	browserify = require("browserify");

	// font_awesome = require("font-awesome");

function browserSync(){
	browsersync.init({
		server:{
			baseDir: "./" + project_folder + "/"
		},
		port : 3000,
		notify : false
	})
}

// gulp.task('fonts', function() {
//   return gulp.src(path.src.fonts)
//   	.pipe(font_awesome())
//     .pipe(gulp.dest(path.build.fonts))
// })

function html(){
	return src(path.src.html)
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream())
}

// gulp.task('skripts', function(){
// 	gulp(path.src.js)
// 	.pipe(dest(path.build.js))
// })

function js(){
	return src(path.src.js)
		.pipe(fileinclude())
		// .pipe(browserify())
		.pipe(dest(path.build.js))
		.pipe(browsersync.stream())
}


function css(){
	return src(path.src.css)
		.pipe(
			sass({
				outputStyle: "expanded"
			})
		)
		.pipe(
			group_media()
		)
		.pipe(
			autoprefixer({
				overrideBrowserList: ["last 5 version"],
				cascade: true
			})
		)
		.pipe(dest(path.build.css))
		.pipe(clean_css())
		.pipe(
			rename({
				extname : ".min.css"
			})
		)
		.pipe(dest(path.build.css))
		.pipe(browsersync.stream())
}

function watchFiles(){
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.js], js);
}

function clean(){
	return del(path.clean);
}


let build = gulp.series(clean, gulp.parallel(js, css, html));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;






















