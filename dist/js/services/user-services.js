

// function getUsers(){
// 	let xhr = new XMLHttpRequest();
// 	xhr.open("GET","https://mag-contacts-api.herokuapp.com/users", true);
// 	return new Promise((onsuccess, oneroror)=>{
// 		xhr.onload = () => onsuccess(xhr.responseText);
// 		xhr.oneroror = () => oneroror(xhr.status)
// 		xhr.send();
// 	});
// }
// getUsers()
// 	.then(resp => JSON.parse(resp))
// 	.then(r=>console.log(r));




// fetch("https://mag-contacts-api.herokuapp.com/users", {
// 	headers:{
// 		"Accept": "application/json"
// 	}, 
// 	method:"GET",
// 	mode:"cors"

// })
// 	.then(r=>r.json())
// 	.then(json=>console.log(json))
// 	.catch(e=> console.log(e));


class UserServices{

	
	getAll(){
		return fetch(UserServices.BASE_URL+'users')
			.then(r=>r.json())
			.then(r=>r.users)
			.then(us=>us.map(u=> User.create(u)))
	}

	register(user){
		return fetch(UserServices.BASE_URL+"register",{
			method:"POST",
			headers:{
				"content-type":"application/json"
			},
			body:JSON.stringify({
				login: user.login,
				password: user.password,
				date_born: user.bornDate
			})
		}).then(r=>r.json());
	}


	login(user){
		return fetch(UserServices.BASE_URL+"login",{
			method:"POST",
			headers:{
				"content-type":"application/json"
			},
			body:JSON.stringify({
				login: user.login,
				password: user.password
			})
		}).then(r=>r.json());
	}


}

UserServices.BASE_URL ='https://mag-contacts-api.herokuapp.com/'; 


